# IRAC PRACTICA 2



1. Punto 3.2
    ```
    node node_modules/node-static/examples/file-server.js
    ```
    Para probar este punto hay 3 endpoints que se han  de probar cada uno en dos browsers (Chrome y Firefox)
    Las pruebas están configuradas para probarlo en local contra el puerto 8089 

    - En local
      - [GetUserMedia](http://localhost:8089/cap23/getUserMedia.html)
      - [LocalPeerConnection](http://localhost:8089/cap23/LocalPeerConnection.html)
      - [dataChannel](http://localhost:8089/cap23/dataChannel.html)
    
    - En AWS
      - [GetUserMedia](http://107.22.6.150:8080/cap23/getUserMedia.html)
      - [LocalPeerConnection](http://107.22.6.150:8080/cap23/LocalPeerConnection.html)
      - [dataChannel](http://107.22.6.150:8080/cap23/dataChannel.html)  

1. Punto 3.3
    ```
    node cap5/completeNodeServerWithDataChannel.js
    ```
    Las pruebas están configuradas para probarlo contra el puerto 8089

    - En local
      - [Acceso a la sala](https://localhost:8089/cap5/)

    - En AWS 
      - [Acceso a la sala](https://107.22.6.150:8080/cap5/)
